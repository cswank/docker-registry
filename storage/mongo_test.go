package storage

import (
	"bitbucket.org/cswank/docker-registry/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"os"
	"testing"
)

func getMongoStorage() (*MongoStorage, error) {
	config := map[string]string{
		"db_name": "dockertest",
		"db_host": "localhost",
	}
	m, err := NewMongoStorage(config)
	m.BufferSize = 24
	return m, err
}

func cleanDB(m *MongoStorage, db *mgo.Database) {
	c := db.C("images")
	c.RemoveAll(bson.M{})
	c = db.C("repositories")
	c.RemoveAll(bson.M{})
	m.Session.Close()
}

func addTestRepository(m *MongoStorage) {
	p := m.TagPath("library", "testrepo", "latest")
	cs := "mychecksum"
	m.PutContent(p, []byte(cs))
	p = m.IndexImagesPath("library", "testrepo")
	ii := []models.IndexImage{
		models.IndexImage{ID: "a", Tag: "b", Checksum: "cs1"},
		models.IndexImage{ID: "c", Tag: "d", Checksum: "cs2"},
		models.IndexImage{ID: "e", Tag: "f", Checksum: "cs3"},
	}
	data, _ := json.Marshal(&ii)
	m.PutContent(p, data)
}

func addTestImage(m *MongoStorage) (*mgo.Database, *mgo.Collection) {
	d := []byte(`{"comment": "Imported from http://get.docker.io/images/base", "container_config": {"Tty": false, "Cmd": null, "MemorySwap": 0, "Image": "", "Hostname": "", "User": "", "Env": null, "Memory": 0, "Detach": false, "Ports": null, "OpenStdin": false}, "id": "27cf784147099545", "created": "2013-03-23T12:53:11.10432-07:00"}`)
	j := &models.ImageJSON{}
	json.Unmarshal(d, j)
	image := &Image{
		ImageID:  "mytestimage",
		Ancestry: []string{"a", "b"},
		Checksum: "mytestimage",
		Json:     j,
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Images)
	c.Insert(image)
	return db, c
}

func TestNewMongoStorage(t *testing.T) {
	m, err := getMongoStorage()
	if err != nil {
		t.Error("didn't ceate a mongostorage", err)
	}
	if m.Repositories != "repositories" {
		t.Error("incorrect repositories value", m.Repositories)
	}
	if m.BufferSize != 24 {
		t.Error("incorrect buffersize", m.BufferSize)
	}
}

func TestMongoCollectionName(t *testing.T) {
	m, _ := getMongoStorage()
	name, p, err := m.getCollectionName("images/fakeimageid/json")
	if name != "images" {
		t.Error("incorrect name", name)
	}
	if p != "fakeimageid/json" {
		t.Error("incorrect path", p)
	}
	if err != nil {
		t.Error(err)
	}
}

func TestMongoPutImageJSON(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.ImageJSONPath("testimage")
	j := &models.ImageJSON{
		ID:      "testimage",
		Created: "never",
		Comment: "none",
	}
	data, _ := json.Marshal(j)
	err := m.PutContent(p, data)
	if err != nil {
		t.Error(err)
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Images)
	i := &Image{}
	err = c.Find(bson.M{"imageID": "testimage"}).One(i)
	if err != nil {
		t.Error(err)
	}
	if i.Json.Created != "never" {
		fmt.Println(i.Json.Created)
		t.Error(i.Json.Created, "does not equal never")
	}
	cleanDB(m, db)
}

func TestMongoPutImageAncestry(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.ImageAncestryPath("testimage")
	l := []string{"xxx", "yyy"}
	data, _ := json.Marshal(l)
	err := m.PutContent(p, data)
	if err != nil {
		t.Error(err)
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Images)
	i := &Image{}
	err = c.Find(bson.M{"imageID": "testimage"}).One(i)
	if err != nil {
		t.Error(err)
	}
	if len(i.Ancestry) != 2 {
		t.Error(i.Ancestry, "does not equal [xxx, yyy]")
	}
	cleanDB(m, db)
}

func TestMongoPutImageChecksum(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.ImageChecksumPath("testimage")
	cs := "mychecksum"
	err := m.PutContent(p, []byte(cs))
	if err != nil {
		t.Error(err)
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Images)
	i := &Image{}
	err = c.Find(bson.M{"imageID": "testimage"}).One(i)
	if err != nil {
		t.Error(err)
	}
	if i.Checksum != cs {
		t.Error(i.Checksum, "does not equal", cs)
	}
	cleanDB(m, db)
}

func TestMongoPutRepositoryTag(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.TagPath("library", "testrepo", "latest")
	cs := "mychecksum"
	err := m.PutContent(p, []byte(cs))
	if err != nil {
		t.Fatal(err)
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Repositories)
	r := &Repository{}
	err = c.Find(bson.M{"namespace": "library", "repository": "testrepo"}).One(r)
	if err != nil {
		t.Fatal(err)
	}
	if r.Tag.ImageID != cs {
		t.Error(r.Tag.ImageID, "does not equal", cs)
	}
	cleanDB(m, db)
}

func TestMongoPutRepositoryIndexImages(t *testing.T) {
	m, _ := getMongoStorage()

	//create an existing doc
	p := m.TagPath("library", "testrepo", "latest")
	cs := "mychecksum"
	m.PutContent(p, []byte(cs))

	//modify it
	p = m.IndexImagesPath("library", "testrepo")
	ii := []models.IndexImage{
		models.IndexImage{ID: "a", Tag: "b", Checksum: "cs1"},
		models.IndexImage{ID: "c", Tag: "d", Checksum: "cs2"},
		models.IndexImage{ID: "e", Tag: "f", Checksum: "cs3"},
	}
	data, _ := json.Marshal(&ii)
	err := m.PutContent(p, data)
	if err != nil {
		t.Fatal(err)
	}
	db := m.Session.DB(m.DBName)
	c := db.C(m.Repositories)
	r := &Repository{}
	err = c.Find(bson.M{"namespace": "library", "repository": "testrepo"}).One(r)
	if err != nil {
		t.Fatal(err)
	}
	if len(r.IndexImages) != 3 {
		t.Error("did not get the right stuff back", r.IndexImages)
	}
	if r.IndexImages[0].ID != "a" {
		t.Error("did not get the right stuff back", r.IndexImages)
	}
	if r.IndexImages[2].Checksum != "cs3" {
		t.Error("did not get the right stuff back", r.IndexImages)
	}

	if r.Tag.ImageID != cs {
		t.Error(r.Tag.ImageID, "does not equal", cs)
	}
	cleanDB(m, db)
}

func TestGetTagContent(t *testing.T) {
	m, _ := getMongoStorage()
	addTestRepository(m)
	p := m.TagPath("library", "testrepo", "latest")
	content, err := m.GetContent(p)
	if err != nil {
		t.Fatal(err)
	}
	tag := &Tag{}
	err = json.Unmarshal(content, tag)
	if err != nil {
		t.Fatal(err)
	}
	if tag.Name != "latest" {
		t.Error(tag.Name, "does not == latest")
	}
	if tag.ImageID != "mychecksum" {
		t.Error(tag.Name, "does not == latest")
	}
	db := m.Session.DB(m.DBName)
	cleanDB(m, db)
}

func TestGetIndexImagesContent(t *testing.T) {
	m, _ := getMongoStorage()
	addTestRepository(m)
	p := m.IndexImagesPath("library", "testrepo")
	content, err := m.GetContent(p)
	if err != nil {
		t.Fatal(err)
	}
	ii := []models.IndexImage{}
	err = json.Unmarshal(content, &ii)
	if err != nil {
		t.Fatal(err)
	}
	if len(ii) != 3 {
		t.Error("should have got 3 indeximages", len(ii))
	}
	if ii[0].ID != "a" {
		t.Error("incorrect indeximages", ii)
	}
	db := m.Session.DB(m.DBName)
	cleanDB(m, db)
}

func TestGetRepositoryTag(t *testing.T) {
	m, _ := getMongoStorage()
	addTestRepository(m)
	p := m.TagPath("library", "testrepo", "latest")
	content, err := m.GetContent(p)
	if err != nil {
		t.Fatal(err)
	}
	tag := Tag{}
	err = json.Unmarshal(content, &tag)
	if err != nil {
		t.Fatal(err)
	}
	if tag.ImageID != "mychecksum" {
		t.Error("incorrect indeximages", tag)
	}
	db := m.Session.DB(m.DBName)
	cleanDB(m, db)
}

func TestGetImageJson(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	jsonPath := m.ImageJSONPath("mytestimage")
	content, err := m.GetContent(jsonPath)
	if err != nil {
		t.Fatal(err)
	}
	j := models.ImageJSON{}
	err = json.Unmarshal(content, &j)
	if err != nil {
		t.Fatal(err)
	}
	if j.ID != "27cf784147099545" {
		t.Error("incorrect indeximages", j.ID)
	}
	cleanDB(m, db)
}

func TestGetImageAncestry(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	jsonPath := m.ImageAncestryPath("mytestimage")
	content, err := m.GetContent(jsonPath)
	if err != nil {
		t.Fatal(err)
	}
	a := []string{}
	err = json.Unmarshal(content, &a)
	if err != nil {
		t.Fatal(err)
	}
	if len(a) != 2 {
		t.Error("incorrect ancestry", a)
	}
	if a[0] != "a" {
		t.Error("incorrect ancestry", a)
	}
	if a[1] != "b" {
		t.Error("incorrect ancestry", a)
	}
	cleanDB(m, db)
}

func TestGetImageAncestrySize(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	jsonPath := m.ImageAncestryPath("mytestimage")
	size, err := m.GetSize(jsonPath)
	if err != nil {
		t.Error(err)
	}
	if size != 9 {
		t.Error(size, "size isn't 9")
	}
	cleanDB(m, db)
}

func TestGetImageChecksum(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	jsonPath := m.ImageChecksumPath("mytestimage")
	content, err := m.GetContent(jsonPath)
	if err != nil {
		t.Fatal(err)
	}
	cs := string(content)
	if cs != "mytestimage" {
		t.Error("incorrect checksum", cs)
	}
	cleanDB(m, db)
}

func TestRepositoryTagExists(t *testing.T) {
	m, _ := getMongoStorage()
	addTestRepository(m)
	p := m.TagPath("library", "testrepo", "latest")
	if !m.Exists(p) {
		t.Error("should have found the tag", p)
	}
	db := m.Session.DB(m.DBName)
	cleanDB(m, db)
}

func TestRepositoryTagExistsNot(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.TagPath("library", "testrepo", "latest")
	if m.Exists(p) {
		t.Error("should have found the tag", p)
	}
}

func TestRepositoryIndexImagesExists(t *testing.T) {
	m, _ := getMongoStorage()
	addTestRepository(m)
	p := m.IndexImagesPath("library", "testrepo")
	if !m.Exists(p) {
		t.Error("should have found the tag", p)
	}
	db := m.Session.DB(m.DBName)
	cleanDB(m, db)
}

func TestRepositoryIndexImagesExistsNot(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.IndexImagesPath("library", "testrepo")
	if m.Exists(p) {
		t.Error("should have found the tag", p)
	}
}

func TestImageAncestryExists(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	p := m.ImageAncestryPath("mytestimage")
	if !m.Exists(p) {
		t.Error("should have found the image ancestry", p)
	}
	cleanDB(m, db)
}

func TestImageAncestryExistsNot(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.ImageAncestryPath("mytestimage")
	if m.Exists(p) {
		t.Error("should have not found the image ancestry", p)
	}
}

func TestImageJsonExists(t *testing.T) {
	m, _ := getMongoStorage()
	db, _ := addTestImage(m)
	p := m.ImageJSONPath("mytestimage")
	if !m.Exists(p) {
		t.Error("should have found the image json", p)
	}
	cleanDB(m, db)
}

func TestImageJsonExistsNot(t *testing.T) {
	m, _ := getMongoStorage()
	p := m.ImageJSONPath("mytestimage")
	if m.Exists(p) {
		t.Error("should have not found the image json", p)
	}
}

func TestMongoStreamWrite(t *testing.T) {
	m, _ := getMongoStorage()
	reader, _ := os.OpenFile("testdata/fluff.txt", os.O_RDONLY, 0644)
	f, err := m.StreamWrite("images/fluff.txt", reader)
	f.Close()
	if err != nil {
		t.Fatal(err)
	}
	data, _ := ioutil.ReadFile("testdata/fluff.txt")
	fi, err := m.Session.DB(m.DBName).GridFS("fs").Open("images/fluff.txt")
	if err != nil {
		t.Fatal(err, "db name", m.DBName)
	}
	writtenData, _ := ioutil.ReadAll(fi)
	if len(writtenData) != len(data) {
		t.Error(len(writtenData), "doesn't equal", len(data))
	}
}

// func _TestMongoInitPath(t *testing.T) {

// }

// func _TestMongoStreamRead(t *testing.T) {
// 	s := getMongoStorage("testdata")
// 	name := "fluff.txt"
// 	orig, _ := ioutil.ReadFile("testdata/fluff.txt")
// 	buf := []byte{}
// 	inCh, outCh := s.StreamRead(name)
// 	for data := range inCh {
// 		buf = append(buf, data...)
// 		outCh <- true
// 	}
// 	if len(orig) != len(buf) {
// 		t.Fatal("didn't get the original file back", string(buf))
// 	}
// 	for i, x := range orig {
// 		if x != buf[i] {
// 			fmt.Println("")
// 			t.Fatal("didn't get the original file back", string(buf))
// 		}
// 	}
// }

// func _TestMongoStreamWrite(t *testing.T) {
// 	s := getMongoStorage("")
//         fi, _:= ioutil.TempFile("", "")
//         tmp := fi.Name()
//         name := path.Base(tmp)
// 	fi, _ = os.OpenFile("testdata/fluff.txt", os.O_RDONLY, 0644)
//         s.StreamWrite(name, fi)
// 	data, _ := ioutil.ReadFile("testdata/fluff.txt")
// 	writtenData, _ := ioutil.ReadFile(tmp)
// 	if len(writtenData) != len(data) {
// 		t.Error(len(writtenData), "doesn't equal", len(data), string(writtenData))
// 	}
// 	os.RemoveAll(tmp)
// }

// func _TestMongoListDirectory(t *testing.T) {
// 	s := getMongoStorage("testdata")
// 	files, err := s.ListDirectory("")
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	if len(files) != 2 {
// 		t.Error("should have found two files", files)
// 	}
// }

// func _TestMongoListBadDirectory(t *testing.T) {
// 	s := getMongoStorage("not_there")
// 	files, err := s.ListDirectory("")
// 	if err == nil {
// 		t.Error("there should have been an error")
// 	}
// 	if len(files) != 0 {
// 		t.Error("should have found no files", files)
// 	}
// }

// func _TestMongoExists(t *testing.T) {
// 	s := getMongoStorage("testdata")
// 	if !s.Exists("fluff.txt") {
// 		t.Error("should have found fluff.txt")
// 	}
// }

// func _TestMongoExistsNot(t *testing.T) {
// 	s := getMongoStorage("testdata")
// 	if s.Exists("junk.txt") {
// 		t.Error("should not have found junk.txt")
// 	}
// }

// func _TestMongoRemove(t *testing.T) {
// 	s := getMongoStorage("")
// 	tmpName := "reg-junk"
// 	tmp, _ := ioutil.TempDir("", tmpName)
// 	tmpName = path.Base(tmp)
// 	fmt.Println(tmp)
// 	for _, _ = range "12345" {
// 		_, _ = ioutil.TempFile(tmp, "")
// 	}
// 	files, _ := s.ListDirectory(tmpName)
// 	if len(files) != 5 {
// 		t.Error("didn't create temp files")
// 	}
// 	s.Remove(tmpName)
// 	files, err := s.ListDirectory(tmpName)
// 	if len(files) != 0 {
// 		t.Error("didn't remove temp files", files, err)
// 	}
// }

// func _TestMongoGetSize(t *testing.T) {
// 	s := getMongoStorage("testdata")
// 	size, err := s.GetSize("fluff.txt")
// 	if size != 960 {
// 		t.Error("the size of fluff.txt is wrong", size)
// 	}
// 	if err != nil {
// 		t.Error(err)
// 	}
// }
