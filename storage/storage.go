package storage

import (
	"bitbucket.org/cswank/docker-registry/models"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

type Storer interface {
	Cleanup()
	GetRepositories() string
	GetImages() string
	GetBufferSize() int
	ImagesListPath(namespace, repository string) string
	ImageJSONPath(imageID string) string
	ImageMarkPath(imageID string) string
	ImageChecksumPath(imageID string) string
	ImageLayerPath(imageID string) string
	ImageAncestryPath(imageID string) string
	TagPath(namespace, repository, tagname string) string
	IndexImagesPath(namespace, repository string) string
	GetContent(path string) ([]byte, error)
	PutContent(path string, content []byte) error
	StreamRead(path string) (models.ReadCloseSeeker, error)
	StreamWrite(path string, reader io.ReadCloser) (models.ReadCloseSeeker, error)
	ListDirectory(path string) ([]string, error)
	Exists(path string) bool
	Remove(path string) error
	GetSize(path string) (int64, error)
}

type Storage struct {
	Repositories string
	Images       string
	BufferSize   int
}

func NewStorage(cfg map[string]string) (Storer, error) {
	if cfg["type"] == "mongo" {
		return NewMongoStorage(cfg)
	} else {
		return NewLocalStorage(cfg)
	}
}

func newStorage() *Storage {
	return &Storage{
		Repositories: "repositories",
		Images:       "images",
		BufferSize:   64 * 1024,
	}
}

func (s *Storage) Cleanup() {}

func (s *Storage) GetBufferSize() int {
	return s.BufferSize
}

func (s *Storage) GetRepositories() string {
	return s.Repositories
}

func (s *Storage) GetImages() string {
	return s.Images
}

func (s *Storage) ImagesListPath(namespace, repository string) string {
	return fmt.Sprintf(
		"%s/%s/%s/_images_list",
		s.Repositories,
		namespace,
		repository,
	)
}

func (s *Storage) ImageJSONPath(imageID string) string {
	return fmt.Sprintf(
		"%s/%s/json",
		s.Images,
		imageID,
	)
}

func (s *Storage) ImageMarkPath(imageID string) string {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	return fmt.Sprintf(
		"%s/%s/_inprogress",
		s.Images,
		imageID,
	)
}

func (s *Storage) ImageChecksumPath(imageID string) string {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	return fmt.Sprintf(
		"%s/%s/_checksum",
		s.Images,
		imageID,
	)
}

func (s *Storage) ImageLayerPath(imageID string) string {
	return fmt.Sprintf(
		"%s/%s/layer",
		s.Images,
		imageID,
	)
}

func (s *Storage) ImageAncestryPath(imageID string) string {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	return fmt.Sprintf(
		"%s/%s/ancestry",
		s.Images,
		imageID,
	)
}

func (s *Storage) TagPath(namespace, repository, tagname string) string {
	if tagname == "" {
		return fmt.Sprintf(
			"%s/%s/%s",
			s.Repositories,
			namespace,
			repository,
		)
	}
	return fmt.Sprintf(
		"%s/%s/%s/tag_%s",
		s.Repositories,
		namespace,
		repository,
		tagname,
	)
}

func (s *Storage) IndexImagesPath(namespace, repository string) string {
	return fmt.Sprintf(
		"%s/%s/%s/_index_images",
		s.Repositories,
		namespace,
		repository,
	)
}

func StoreStream(reader io.ReadCloser) (*os.File, error) {
	fi, err := ioutil.TempFile("", "")
	fmt.Println("store stream path")
	fmt.Println(fi.Name())
	if err != nil {
		fmt.Println("error in tempfile", err)
		return fi, err
	}
	buf := make([]byte, 4096)
	for {
		n, e := io.ReadFull(reader, buf)
		fi.Write(buf[:n])
		//if n < 4096 || e != nil {
		if e != nil {
			break
		}
	}
	fi.Close()
	reader.Close()
	outFi, _ := os.Open(fi.Name())
	return outFi, nil
}
