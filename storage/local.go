package storage

import (
	"bitbucket.org/cswank/docker-registry/models"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

type LocalStorage struct {
	Storage
	Config   map[string]string
	RootPath string
}

func NewLocalStorage(config map[string]string) (*LocalStorage, error) {
	return &LocalStorage{
		Storage: Storage{
			Repositories: "repositories",
			Images:       "images",
			BufferSize:   64 * 1024,
		},
		Config:   config,
		RootPath: config["storage_path"],
	}, nil
}

func exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func (s *LocalStorage) initPath(path string, create bool) string {
	if path == "" {
		path = s.RootPath
	} else if len(path) >= len(s.RootPath) && path[:len(s.RootPath)] == s.RootPath {
		path = path
	} else {
		path = filepath.Join(s.RootPath, path)
	}
	if create {
		dirname := filepath.Dir(path)
		if !exists(dirname) {
			os.MkdirAll(dirname, 0777)
		}
	}
	return path
}

func (s *LocalStorage) GetContent(path string) ([]byte, error) {
	path = s.initPath(path, false)
	return ioutil.ReadFile(path)
}

func (s *LocalStorage) PutContent(path string, content []byte) error {
	path = s.initPath(path, true)
	return ioutil.WriteFile(path, content, 0644)
}

func (s *LocalStorage) StreamRead(path string) (models.ReadCloseSeeker, error) {
	path = s.initPath(path, false)
	return os.Open(path)
}

func (s *LocalStorage) StreamWrite(path string, reader io.ReadCloser) (models.ReadCloseSeeker, error) {
	buf := make([]byte, s.BufferSize)
	path = s.initPath(path, true)
	fi, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		fi.Close()
		return fi, err
	}
	defer fi.Close()
	defer reader.Close()
	for {
		n, err := reader.Read(buf)
		if err != nil {
			break
		}
		n, err = fi.Write(buf[:n])
		if err != nil {
			break
		}
	}
	return os.Open(path)
}

func (s *LocalStorage) ListDirectory(path string) ([]string, error) {
	path = s.initPath(path, true)
	fileInfos, err := ioutil.ReadDir(path)
	var files []string
	if err == nil {
		files = make([]string, len(fileInfos))
		for i, f := range fileInfos {
			files[i] = filepath.Join(path, f.Name())
		}
	}
	return files, err
}

func (s *LocalStorage) Exists(path string) bool {
	path = s.initPath(path, true)
	if _, err := os.Stat(path); err == nil {
		return true
	}
	return false
}

func (s *LocalStorage) Remove(path string) error {
	path = s.initPath(path, true)
	return os.RemoveAll(path)
}

func (s *LocalStorage) GetSize(path string) (int64, error) {
	path = s.initPath(path, true)
	f, err := os.OpenFile(path, os.O_RDONLY, 0)
	var size int64
	if err == nil {
		fi, _ := f.Stat()
		f.Close()
		size = fi.Size()
	}
	return size, err
}
