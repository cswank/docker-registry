package storage

import (
	"bitbucket.org/cswank/docker-registry/models"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"strings"
)

type MongoStorage struct {
	Storage
	Config  map[string]string
	DBName  string
	DBHost  string
	Session *mgo.Session
}

func NewMongoStorage(config map[string]string) (*MongoStorage, error) {
	session, err := mgo.Dial(config["db_host"])
	session.SetSafe(&mgo.Safe{})
	return &MongoStorage{
		Storage: Storage{
			Repositories: "repositories",
			Images:       "images",
			BufferSize:   64 * 1024,
		},
		Config:  config,
		DBName:  config["db_name"],
		DBHost:  config["db_host"],
		Session: session,
	}, err
}

type Tag struct {
	Name    string
	ImageID string
}

type Image struct {
	ID         bson.ObjectId     `bson:"_id,omitempty"`
	ImageID    string            `bson:"imageID"`
	Ancestry   []string          `bson:"ancestry"`
	Checksum   string            `bson:"_checksum"`
	InProgress string            `bson:"_inprogress"`
	Json       *models.ImageJSON `bson:"json"`
	Layer      bson.ObjectId     `bson:"layer,omitempty"`
}

type Repository struct {
	ID          bson.ObjectId       `bson:"_id,omitempty"`
	Namespace   string              `bson:"namespace"`
	Repository  string              `bson:"repository"`
	IndexImages []models.IndexImage `bson:"_index_images"`
	Tag         *Tag                `bson:"tag"`
}

/*
the paths should follow this:

/images/imageID/key
/repositories/namespace/repository/key

with special cases if the key starts with 'tag_'
*/

func (m *MongoStorage) Cleanup() {
	m.Session.Close()
}

func (m *MongoStorage) GetContent(p string) (content []byte, err error) {
	collectionName, p, err := m.getCollectionName(p)
	if err != nil {
		return content, err
	}
	if collectionName == m.Images {
		content, err = m.getImageContent(p)
	} else if collectionName == m.Repositories {
		content, err = m.getRepositoryContent(p)
	}
	return content, err
}

func (m *MongoStorage) getImageContent(p string) (content []byte, err error) {
	imageID, key, err := m.splitImagePath(p)
	if err != nil {
		return content, err
	}
	image, err := m.getImage(imageID)
	if err != nil {
		return content, err
	}
	return m.getContentFromImage(image, key, p)
}

func (m *MongoStorage) getContentFromImage(image *Image, key, p string) (content []byte, err error) {
	if key == "json" {
		content, err = json.Marshal(image.Json)
	} else if key == "ancestry" {
		content, err = json.Marshal(image.Ancestry)
	} else if key == "_checksum" {
		content = []byte(image.Checksum)
		if len(content) == 0 {
			err = errors.New(fmt.Sprintf("not found: %s", p))
		}
	} else if key == "_inprogress" {
		content = []byte(image.InProgress)
		if len(content) == 0 {
			err = errors.New(fmt.Sprintf("not found: %s", p))
		}
	} else {
		err = errors.New(fmt.Sprintf("invalid path: %s", p))
	}
	return content, err
}

func (m *MongoStorage) getRepositoryContent(p string) (content []byte, err error) {
	namespace, repoName, key, err := m.splitRepoPath(p)
	if err != nil {
		return content, err
	}
	repository, err := m.getRepository(namespace, repoName)
	if err != nil {
		return content, err
	}
	return m.getContentFromRepository(repository, key, p)
}

func (m *MongoStorage) getContentFromRepository(repository *Repository, key, p string) (content []byte, err error) {
	if key == "_index_images" {
		content, err = json.Marshal(repository.IndexImages)
	} else if strings.Index(key, "tag_") == 0 {
		content, err = json.Marshal(repository.Tag)
	} else {
		err = errors.New(fmt.Sprintf("invalid p: %s", p))
	}
	return content, err
}

func (m *MongoStorage) getImage(imageID string) (*Image, error) {
	i := &Image{}
	c := m.Session.DB(m.DBName).C(m.Images)
	err := c.Find(bson.M{"imageID": imageID}).One(i)
	return i, err
}

func (m *MongoStorage) getRepository(namespace, repoName string) (*Repository, error) {
	r := &Repository{}
	c := m.Session.DB(m.DBName).C(m.Repositories)
	err := c.Find(bson.M{"namespace": namespace, "repository": repoName}).One(r)
	return r, err
}

func (m *MongoStorage) PutContent(p string, content []byte) error {
	collectionName, p, err := m.getCollectionName(p)
	if err != nil {
		return err
	}
	if collectionName == m.Images {
		return m.putImage(p, content)
	} else if collectionName == m.Repositories {
		return m.putRepository(p, content)
	}
	return nil
}

func (m *MongoStorage) splitRepoPath(p string) (namespace, repoName, key string, err error) {
	parts := strings.Split(p, "/")
	if len(parts) != 3 {
		err = errors.New(fmt.Sprintf("invalid path: %s", parts))
	} else {
		namespace = parts[0]
		repoName = parts[1]
		key = parts[2]
	}
	return namespace, repoName, key, err
}

func (m *MongoStorage) putRepository(p string, content []byte) error {
	namespace, repoName, key, err := m.splitRepoPath(p)
	if err != nil {
		return err
	}
	var changes bson.M
	if key == "_index_images" {
		changes, err = m.putIndexImages(content)
	} else if strings.Index(key, "tag_") == 0 {
		changes, err = m.putTag(key, content)
	} else {
		err = errors.New(fmt.Sprintf("invalid path: %s", p))
	}
	if err == nil {
		c := m.Session.DB(m.DBName).C(m.Repositories)
		_, err = c.Upsert(bson.M{"namespace": namespace, "repository": repoName}, changes)
	}
	return err
}

func (m *MongoStorage) putIndexImages(content []byte) (bson.M, error) {
	ii := []models.IndexImage{}
	err := json.Unmarshal(content, &ii)
	var changes bson.M
	if err == nil {
		changes = bson.M{"$set": bson.M{"_index_images": ii}}
	}
	return changes, err
}

func (m *MongoStorage) putTag(key string, content []byte) (bson.M, error) {
	name := key[4:]
	t := &Tag{Name: name, ImageID: string(content)}
	return bson.M{"$set": bson.M{"tag": t}}, nil
}

func (m *MongoStorage) splitImagePath(p string) (imageID, key string, err error) {
	parts := strings.Split(p, "/")
	if len(parts) != 2 {
		err = errors.New(fmt.Sprintf("invalid path: %s", p))
	} else {
		imageID = parts[0]
		key = parts[1]
	}
	return imageID, key, err
}

func (m *MongoStorage) putImage(p string, content []byte) error {
	imageID, key, err := m.splitImagePath(p)
	if err != nil {
		return err
	}
	var changes bson.M
	if key == "json" {
		changes, err = m.putImageJSON(content)
	} else if key == "ancestry" {
		changes, err = m.putImageAncestry(content)
	} else if key == "_checksum" {
		changes = bson.M{"$set": bson.M{"_checksum": string(content)}}
	} else if key == "_inprogress" {
		changes = bson.M{"$set": bson.M{"_inprogress": string(content)}}
	} else {
		err = errors.New(fmt.Sprintf("invalid path: %s", p))
	}
	if err == nil {
		c := m.Session.DB(m.DBName).C(m.Images)
		_, err = c.Upsert(bson.M{"imageID": imageID}, changes)
	}
	return err
}

func (m *MongoStorage) putImageJSON(content []byte) (bson.M, error) {
	j := &models.ImageJSON{}
	err := json.Unmarshal(content, j)
	return bson.M{"$set": bson.M{"json": j}}, err
}

func (m *MongoStorage) putImageAncestry(content []byte) (bson.M, error) {
	l := []string{}
	err := json.Unmarshal(content, &l)
	return bson.M{"$set": bson.M{"ancestry": l}}, err
}

func (m *MongoStorage) StreamRead(p string) (models.ReadCloseSeeker, error) {
	return m.Session.DB(m.DBName).GridFS("fs").Open(p)
}

func (m *MongoStorage) StreamWrite(p string, reader io.ReadCloser) (tmp models.ReadCloseSeeker, err error) {
	fi, err := m.Session.DB(m.DBName).GridFS("fs").Create(p)
	if err != nil {
		return tmp, err
	}
	buf := make([]byte, m.BufferSize)
	for {
		n, err := reader.Read(buf)
		fi.Write(buf[:n])
		if err != nil {
			break
		}
	}
	return fi, err
}

func (m *MongoStorage) ListDirectory(p string) ([]string, error) {
	return []string{}, nil
}

func (m *MongoStorage) Exists(p string) bool {
	collectionName, p, err := m.getCollectionName(p)
	if err != nil {
		return false
	}
	if collectionName == m.Images {
		return m.existsImage(p)
	} else if collectionName == m.Repositories {
		return m.existsRepository(p)
	}
	return false
}

func (m *MongoStorage) existsImage(p string) bool {
	imageID, key, err := m.splitImagePath(p)
	if err != nil {
		return false
	}
	image, err := m.getImage(imageID)
	if err != nil {
		return false
	}
	content, _ := m.getContentFromImage(image, key, p)
	return string(content) != "null"
}

func (m *MongoStorage) existsRepository(p string) bool {
	namespace, repoName, key, err := m.splitRepoPath(p)
	if err != nil {
		return false
	}
	repository, err := m.getRepository(namespace, repoName)
	if err != nil {
		return false
	}
	content, err := m.getContentFromRepository(repository, key, p)
	return err == nil && len(content) > 0
}

func (m *MongoStorage) Remove(p string) error {
	content := []byte{}
	return m.PutContent(p, content)
}

func (m *MongoStorage) GetSize(p string) (size int64, err error) {
	content, err := m.GetContent(p)
	if err == nil {
		size = int64(len(content))
	}
	return size, err
}

func (m *MongoStorage) getCollectionName(p string) (string, string, error) {
	if strings.Index(p, m.Repositories) == 0 && len(p) > len(m.Repositories)+1 {
		return m.Repositories, p[len(m.Repositories)+1:], nil
	} else if strings.Index(p, m.Images) == 0 && len(p) > len(m.Images)+1 {
		return m.Images, p[len(m.Images)+1:], nil
	}
	return "", "", errors.New("invalid path")
}
