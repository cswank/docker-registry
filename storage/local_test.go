package storage

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func getLocalStorage(path string) *LocalStorage {
	if path == "" {
		path = "/tmp"
	}
	config := map[string]string{
		"storage_path": path,
	}
	l, _ := NewLocalStorage(config)
	l.BufferSize = 24
	return l
}

func TestNewLocalStorage(t *testing.T) {
	config := map[string]string{
		"storage_path": "/tmp",
	}
	l, _ := NewLocalStorage(config)
	if l == nil {
		t.Error("didn't ceate a localstorage", l)
	}
	if l.Repositories != "repositories" {
		t.Error("incorrect repositories value", l.Repositories)
	}
	if l.BufferSize != 64*1024 {
		t.Error("incorrect buffersize", l.BufferSize)
	}
}

func TestInitPath(t *testing.T) {

}

// func TestStreamRead(t *testing.T) {
// 	s := getLocalStorage("testdata")
// 	name := "fluff.txt"
// 	orig, _ := ioutil.ReadFile("testdata/fluff.txt")
// 	buf := []byte{}
// 	inCh, outCh := s.StreamRead(name)
// 	for data := range inCh {
// 		buf = append(buf, data...)
// 		outCh <- true
// 	}
// 	if len(orig) != len(buf) {
// 		t.Fatal("didn't get the original file back", string(buf))
// 	}
// 	for i, x := range orig {
// 		if x != buf[i] {
// 			fmt.Println("")
// 			t.Fatal("didn't get the original file back", string(buf))
// 		}
// 	}
// }

func TestStreamWrite(t *testing.T) {
	s := getLocalStorage("")
	tmpFile, _ := ioutil.TempFile("/tmp", "")
	tmp := tmpFile.Name()
	//name := path.Base(tmp)
	data, _ := ioutil.ReadFile("testdata/fluff.txt")
	fi, _ := os.OpenFile("testdata/fluff.txt", os.O_RDONLY, 0644)
	f, err := s.StreamWrite(tmp, fi)
	if err != nil {
		t.Error(err)
	}
	f.Close()
	writtenData, _ := ioutil.ReadFile(tmp)
	if len(writtenData) != len(data) {
		t.Error(len(writtenData), "doesn't equal", len(data), string(writtenData))
	}
	os.RemoveAll(tmp)
}

func TestListDirectory(t *testing.T) {
	s := getLocalStorage("testdata")
	files, err := s.ListDirectory("")
	if err != nil {
		t.Error(err)
	}
	if len(files) != 1 {
		t.Error("should have found two files", files)
	}
}

func TestListBadDirectory(t *testing.T) {
	s := getLocalStorage("not_there")
	files, err := s.ListDirectory("")
	if err == nil {
		t.Error("there should have been an error")
	}
	if len(files) != 0 {
		t.Error("should have found no files", files)
	}
}

func TestExists(t *testing.T) {
	s := getLocalStorage("testdata")
	if !s.Exists("fluff.txt") {
		t.Error("should have found fluff.txt")
	}
}

func TestExistsNot(t *testing.T) {
	s := getLocalStorage("testdata")
	if s.Exists("junk.txt") {
		t.Error("should not have found junk.txt")
	}
}

func TestRemove(t *testing.T) {
	tmp, _ := ioutil.TempDir("", "")
	s := getLocalStorage(tmp)
	dirname := "junk"
	pth := path.Join(tmp, dirname)
	os.MkdirAll(pth, 0777)
	for _, _ = range "12345" {
		_, _ = ioutil.TempFile(pth, "")
	}
	files, err := s.ListDirectory(dirname)
	if err != nil {
		t.Fatal(err)
	}
	if len(files) != 5 {
		t.Error("didn't create temp files", len(files), dirname)
	}
	s.Remove(dirname)
	files, err = s.ListDirectory(dirname)
	if len(files) != 0 {
		t.Error("didn't remove temp files", files, err)
	}
}

func TestGetSize(t *testing.T) {
	s := getLocalStorage("testdata")
	size, err := s.GetSize("fluff.txt")
	if size != 1300 {
		t.Error("the size of fluff.txt is wrong", size)
	}
	if err != nil {
		t.Error(err)
	}
}
