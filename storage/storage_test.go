package storage

import (
	"testing"
)

func TestNewStorage(t *testing.T) {
	s := newStorage()
	if s.Repositories != "repositories" {
		t.Error("storage didn't get created correctly", s)
	}
}

func TestImagesListPath(t *testing.T) {
	s := newStorage()
	p := s.ImagesListPath("tmp", "junk")
	if p != "repositories/tmp/junk/_images_list" {
		t.Error("the path wasn't correct", p)
	}
}

func TestImageJSONPath(t *testing.T) {
	//{0}/{1}/json.format(self.image, image_id)
	s := newStorage()
	p := s.ImageJSONPath("junk")
	if p != "images/junk/json" {
		t.Error("the image path wasn't correct", p)
	}
}

func TestImageMarkPath(t *testing.T) {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	s := newStorage()
	p := s.ImageMarkPath("junk")
	if p != "images/junk/_inprogress" {
		t.Error("the image path wasn't correct", p)
	}
}

func TestImageChecksumPath(t *testing.T) {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	s := newStorage()
	p := s.ImageChecksumPath("junk")
	if p != "images/junk/_checksum" {
		t.Error("the image path wasn't correct", p)
	}
}

func TestAncestryPath(t *testing.T) {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	s := newStorage()
	p := s.ImageAncestryPath("junk")
	if p != "images/junk/ancestry" {
		t.Error("the image path wasn't correct", p)
	}
}

func TestTagPath(t *testing.T) {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	s := newStorage()
	p := s.TagPath("tmp", "stuff", "")
	if p != "repositories/tmp/stuff" {
		t.Error("the image path wasn't correct", p)
	}
	p = s.TagPath("tmp", "stuff", "latest")
	if p != "repositories/tmp/stuff/tag_latest" {
		t.Error("the image path wasn't correct", p)
	}
}

func TestIndexImagesPath(t *testing.T) {
	//{0}/{1}/_inprogress'.format(self.image, image_id)
	s := newStorage()
	p := s.IndexImagesPath("tmp", "stuff")
	if p != "repositories/tmp/stuff/_index_images" {
		t.Error("the image path wasn't correct", p)
	}
}
