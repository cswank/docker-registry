package checksums

import (
	"fmt"
	"os"
	"testing"
)

var jsonString = []byte(`{"container": "3d67245a8d72ecf13f33dffac9f79dcdf70f75acb84d308770391510e0c23ad0", "parent": "27cf784147099545", "created": "2013-03-23T22:24:18.818426-07:00", "container_config": {"Tty": true, "Cmd": ["/bin/bash"], "Env": null, "Image": "base", "Hostname": "", "User": "", "MemorySwap": 0, "Memory": 0, "Detach": false, "Ports": null, "OpenStdin": true}, "id": "b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc"}`)

func TestComputeTarsum(t *testing.T) {
	expected := "tarsum+sha256:4be36c041020d529a6300a4eb9e75d7c02d54aa7192f2e7e798f5b256decfc1a"
	f, _ := os.Open("testdata/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/layer")
	sum, err := ComputeTarsum(f, jsonString)
	if err != nil {
		t.Error(err)
	}
	if sum != expected {
		fmt.Println("")
		t.Error(sum, "does not equal", expected)
	}
}

func TestSha265String(t *testing.T) {
	expected := "c157a0b0d40f9d9506c72fae584069d1692b816da16cd76c648d5761c588057a"
	result := sha256String([]byte("craig"))
	if result != expected {
		fmt.Println("")
		t.Error(result, "does not equal", expected)
	}
}

func TestComputeSimple(t *testing.T) {
	expected := "sha256:c1312527f2c6b508e79aaaeb78f4c1f93da409c411d87b11418c67d27b929c77"
	f, _ := os.Open("testdata/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/layer")
	result := ComputeSimple(f, jsonString)
	if result != expected {
		t.Error(result, "does not equal", expected)
	}
}
