package checksums

import (
	"archive/tar"
	"compress/gzip"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"sort"
	"strings"
)

func getHeader(hdr *tar.Header) []byte {
	return []byte(fmt.Sprintf("name%smode%duid%dgid%dsize%dmtime%dtypeflag%clinkname%suname%sgname%sdevmajor%ddevminor%d",
		hdr.Name, hdr.Mode, hdr.Uid, hdr.Gid, hdr.Size,
		hdr.ModTime.Unix(), hdr.Typeflag, hdr.Linkname, hdr.Uname,
		hdr.Gname, hdr.Devmajor, hdr.Devminor))
}

func sha256File(reader io.Reader, data []byte) string {
	h := sha256.New()
	h.Write(data)
	buf := make([]byte, 4096)
	for {
		n, err := reader.Read(buf)
		h.Write(buf[:n])
		if n < 4096 || err != nil {
			break
		}
	}
	return hex.EncodeToString(h.Sum(nil))
}

func sha256String(data []byte) string {
	h := sha256.New()
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}

func ComputeTarsum(f io.Reader, jsonData []byte) (string, error) {
	zhandle, err := gzip.NewReader(f)
	if err != nil {
		return "", err
	}
	reader := tar.NewReader(zhandle)

	hashes := []string{}
	for hdr, err := reader.Next(); err == nil; hdr, err = reader.Next() {
		var hash string
		header := getHeader(hdr)
		if hdr.Size > 0 {
			hash = sha256File(reader, header)
		} else {
			hash = sha256String(header)

		}
		hashes = append(hashes, hash)
	}
	sort.Strings(hashes)
	data := append(jsonData, []byte(strings.Join(hashes, ""))...)
	tarsum := fmt.Sprintf("tarsum+sha256:%s", sha256String(data))
	return tarsum, nil
}

func ComputeSimple(reader io.Reader, jsonData []byte) string {
	return fmt.Sprintf(
		"sha256:%s",
		sha256File(reader, append(jsonData, []byte("\n")...)),
	)
}
