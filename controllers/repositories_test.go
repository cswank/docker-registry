package controllers

import (
	"bitbucket.org/cswank/docker-registry/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"testing"
)

var (
	testImagesData = []byte(`[{"id":"27cf784147099545","Tag":"latest"},{"id":"b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc","Tag":"latest"},{"id":"215a1daba1c25077854acdb41f92ad0ea5d735c1c4737a78aa945e218ae0b1de","Tag":"latest"}]`)
	testImages     []models.IndexImage
)

func init() {
	json.Unmarshal(testImagesData, &testImages)
}

func getIndexImages(pth string) []models.IndexImage {
	b, _ := ioutil.ReadFile(path.Join(cfg["storage_path"], pth))
	i := []models.IndexImage{}
	json.Unmarshal(b, &i)
	return i
}

func _TestPutRepository(t *testing.T) {
	req, _ := getTestRequest("PUT", "http://example.com/v1/repositories/base", bytes.NewBuffer(testImagesData))
	err, status := PutRepository(req)
	if err != nil {
		t.Error(err)
	}
	if status != 200 {
		t.Error(status)
	}
	i := getIndexImages("repositories/library/base/_index_images")
	if i[0].ID != "27cf784147099545" {
		t.Error(i[0])
	}
	cleanupTest()
}

func _TestGetRepository(t *testing.T) {
	req, w := getTestRequest("PUT", "http://example.com/v1/repositories/xyz", bytes.NewBuffer(testImagesData))
	PutRepository(req)

	req, w = getTestRequest("GET", "http://example.com/v1/repositories/xyz", nil)
	err, status := GetRepository(req)

	if err != nil {
		t.Error(err)
	}

	expected := `[{"id":"27cf784147099545","Tag":"latest","checksum":""},{"id":"b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc","Tag":"latest","checksum":""},{"id":"215a1daba1c25077854acdb41f92ad0ea5d735c1c4737a78aa945e218ae0b1de","Tag":"latest","checksum":""}]`
	actual := string(w.Body.Bytes())
	if actual != expected {
		t.Error(actual)
	}
	if status != 200 {
		t.Error(status)
	}
	cleanupTest()
}

func _TestGetRepositoryNotFound(t *testing.T) {
	req, _ := getTestRequest("GET", "http://example.com/v1/repositories/xyz", nil)

	err, code := GetRepository(req)
	if code != 404 {
		t.Error(code)
	}
	if err == nil {
		t.Error("should have had an error")
	}
	cleanupTest()
}

func _TestParseRepository(t *testing.T) {
	namespace, repo, images, err := parseRepository("base")
	if err != nil {
		t.Error(err)
	}
	if namespace != "library" {
		t.Error("the namespace should have been library, not", namespace)
	}
	if repo != "base" {
		t.Error("the repository should have been base, not", repo)
	}
	if images != "" {
		t.Error("images should have been an empty string, not", images)
	}
}

// func getTestStorageApplication() (*StorageApplication, string) {
// 	tmp, _ := ioutil.TempDir("", "")
// 	config := map[string]string{
// 		"storage_path": tmp,
// 	}
// 	l := storage.NewLocalStorage(config)
// 	c := &revel.Controller{Response: &revel.Response{}}
// 	a := Application{c}
// 	return &StorageApplication{
// 		Application: a,
// 		Storage: l,
// 	}, tmp
// }

// func TestSaveIndexImage(t *testing.T) {
// 	s, tmp := getTestStorageApplication()
// 	p := s.Storage.IndexImagesPath("library", "gobase")
// 	i := []IndexImage{}
// 	e := []IndexImage{}
// 	json.Unmarshal(testImages, &i)
// 	err := s.saveIndexImages(i, e, p)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	writtenData, err := s.Storage.GetContent(p)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	loaded := []IndexImage{}
// 	err = json.Unmarshal(writtenData, &loaded)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	if len(loaded) != 3 {
// 		t.Error("should have written 3 images to disk", loaded)
// 	}
// 	for _, image := range loaded {
// 		if image.ID == "" {
// 			t.Error("the images didn't get written correctly", image)
// 		}
// 	}
// 	os.RemoveAll(tmp)
// }

// func TestFilterIndexImagesWithChecksums(t *testing.T) {
// 	i := []IndexImage{}
// 	e := []IndexImage{}
// 	json.Unmarshal(testImages, &i)
// 	json.Unmarshal(testImages, &e)
// 	for j, image := range e {
// 		image.Checksum = randomString(16)
// 		e[j] = image
// 	}
// 	filtered := filterIndexImages(i, e)
// 	if len(filtered) != 3 {
// 		t.Error("the images didn't get filtered", filtered)
// 	}
// 	for _, i := range filtered {
// 		if i.Checksum == "" {
// 			t.Error("only images with checksums should exist", i)
// 		}
// 	}
// }

func TestFilterIndexImages(t *testing.T) {
	i := []models.IndexImage{}
	e := []models.IndexImage{}
	json.Unmarshal(testImagesData, &i)
	json.Unmarshal(testImagesData, &e)
	filtered := filterIndexImages(i, e)
	if len(filtered) != 3 {
		t.Error("the images didn't get filtered", filtered)
	}
}

func TestFilterIndexMoreImagesWithChecksums(t *testing.T) {
	i := []models.IndexImage{}
	e := []models.IndexImage{}
	json.Unmarshal(testImagesData, &i)
	json.Unmarshal(testImagesData, &e)
	for j, image := range e {
		image.Checksum = randomString(16)
		image.ID = fmt.Sprintf("%d", j)
		e[j] = image
	}
	filtered := filterIndexImages(i, e)
	if len(filtered) != 6 {
		t.Error("the images didn't get filtered", filtered)
	}
}
