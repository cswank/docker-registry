package controllers

import (
	"encoding/json"
	"errors"
	"path"
)

func PutTag(r *Request) (error, int) {
	r.namespace = "library"
	return PutTagWithNamespace(r)
}

func PutTagWithNamespace(r *Request) (error, int) {
	data, _ := r.readBody()
	defer r.close()
	imageID := ""
	err := json.Unmarshal(data, &imageID)
	if err != nil {
		return errors.New("Invalid data"), 400
	}
	if !r.storage.Exists(r.storage.ImageJSONPath(imageID)) {
		return errors.New("Image not found"), 404
	}
	r.storage.PutContent(r.storage.TagPath(r.namespace, r.repository, r.tag), []byte(imageID))
	return nil, 200
}

func GetTag(r *Request) (error, int) {
	r.namespace = "library"
	return GetTagWithNamespace(r)
}

func GetTagWithNamespace(r *Request) (error, int) {
	p := r.storage.TagPath(r.namespace, r.repository, r.tag)
	data, err := r.storage.GetContent(p)
	if err != nil {
		return errors.New("Tag not found"), 404
	}
	r.w.Write(data)
	return nil, 200
}

func GetTagsWithNamespace(r *Request) (error, int) {
	p := r.storage.TagPath(r.namespace, r.repository, "")
	tags, err := r.storage.ListDirectory(p)
	if err != nil {
		return errors.New("Repository not found"), 404
	}
	output := make(map[string]string)
	for _, fname := range tags {
		tagName := path.Base(fname)
		if tagName[:4] != "tag_" {
			continue
		}
		data, err := r.storage.GetContent(fname)
		if err != nil {
			return err, 500
		}
		output[tagName[4:]] = string(data)
	}
	d, _ := json.Marshal(&output)
	r.w.Write(d)
	return nil, 200
}
