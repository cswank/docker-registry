package controllers

import (
	"bitbucket.org/cswank/docker-registry/storage"
	"github.com/gorilla/sessions"
	"io"
	"net/http"
	"os"
)

var (
	cfg map[string]string
)

func init() {
	storage := os.Getenv("REGISTRY_STORAGE")
	if storage == "mongo" {
		initMongo()
	} else {
		initLocal()
	}
}

func initLocal() {
	storagePath := os.Getenv("REGISTRY_STORAGE_PATH")
	if storagePath == "" {
		storagePath = "/tmp/docker-registry"
	}
	cfg = map[string]string{
		"type":         "local",
		"storage_path": storagePath,
	}
}

func initMongo() {
	db := os.Getenv("REGISTRY_STORAGE_DB")
	if db == "" {
		db = "docker-registry"
	}
	host := os.Getenv("REGISTRY_STORAGE_HOST")
	if host == "" {
		host = "localhost"
	}
	cfg = map[string]string{
		"type":    "mongo",
		"db_name": db,
		"db_host": host,
	}
}

type Request struct {
	w          http.ResponseWriter
	r          *http.Request
	storage    storage.Storer
	session    *sessions.Session
	namespace  string
	repository string
	imageId    string
	tag        string
}

func NewRequest(w http.ResponseWriter, r *http.Request, vars map[string]string) (*Request, error) {
	s, err := storage.NewStorage(cfg)
	if err != nil {
		return nil, err
	}
	session, _ := store.Get(r, "docker-session")
	return &Request{
		w,
		r,
		s,
		session,
		vars["namespace"],
		vars["repository"],
		vars["imageId"],
		vars["tag"],
	}, nil
}

func (r *Request) SaveSession() {
	r.session.Save(r.r, r.w)
}

func (r *Request) writeJSON(data []byte) (error, int) {
	r.w.Header().Add("Content-Type", "application/json")
	r.w.Write(data)
	return nil, 200
}

func (r *Request) close() {
	r.r.Body.Close()
}

func (r *Request) readBody() ([]byte, error) {
	buf := make([]byte, r.storage.GetBufferSize())
	body := []byte{}
	for {
		n, err := io.ReadFull(r.r.Body, buf)
		body = append(body, buf[:n]...)
		if n < r.storage.GetBufferSize() || err != nil {
			break
		}
	}
	return body, nil
}

func Index() (error, int) {
	return nil, 200
}

func Ping(r *Request) (error, int) {
	r.w.Write([]byte(`<html>
  <head>
  </head>
  <body>
    <pre style="word-wrap: break-word; white-space: pre-wrap;">true</pre>
  </body>
</html>
`))
	return nil, 200
}
