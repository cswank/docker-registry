package controllers

import (
	"bitbucket.org/cswank/docker-registry/checksums"
	"bitbucket.org/cswank/docker-registry/models"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/sessions"
	"io"
	"strings"
)

var (
	store = sessions.NewCookieStore([]byte("3jzldf0tyz2jdkl2jdlkj"))
)

func PutImageJSON(r *Request) (error, int) {
	err := storeChecksum(r)
	if err != nil {
		return err, 400
	}
	err = storeImageJSON(r)
	if err != nil {
		return err, 400
	}
	return nil, 200
}

func PutImageLayer(r *Request) (error, int) {
	_, err := r.storage.GetContent(r.storage.ImageJSONPath(r.imageId))
	if err != nil {
		return err, 500
	}
	layerPath := r.storage.ImageLayerPath(r.imageId)
	markPath := r.storage.ImageMarkPath(r.imageId)
	if r.storage.Exists(layerPath) && !r.storage.Exists(markPath) {
		return errors.New("image already exists"), 400
	}
	f, err := r.storage.StreamWrite(layerPath, r.r.Body)
	defer r.r.Body.Close()
	if err != nil {
		return err, 400
	}
	defer f.Close()
	return nil, 200
	//return checkChecksums(f, markPath, jsonData, r)
}

func checkChecksums(f io.ReadSeeker, markPath string, jsonData []byte, r *Request) (error, int) {
	csums := []string{checksums.ComputeSimple(f, jsonData)}
	f.Seek(0, 0)
	cs, err := checksums.ComputeTarsum(f, jsonData)
	if err != nil {
		return err, 400
	}
	csums = append(csums, cs)
	pth := r.storage.ImageChecksumPath(r.imageId)
	checksum, err := r.storage.GetContent(pth)
	if err != nil {
		cs, _ := json.Marshal(csums)
		r.session.Values["checksum"] = string(cs)
		r.SaveSession()
	} else {

	}
	if !in(string(checksum), csums) {
		return errors.New("Checksum mismatch, ignoring the layer"), 400
	}
	r.storage.Remove(markPath)
	return nil, 200
}

func GetImageLayer(r *Request) (error, int) {
	path := r.storage.ImageLayerPath(r.imageId)
	f, err := r.storage.StreamRead(path)
	defer f.Close()
	if err != nil {
		return errors.New("Image not found"), 404
	}
	io.Copy(r.w, f)
	return nil, 200
}

func GetImageAncestry(r *Request) (error, int) {
	p := r.storage.ImageAncestryPath(r.imageId)
	data, err := r.storage.GetContent(p)
	if err != nil {
		return errors.New("Image not found"), 404
	}
	r.w.Write(data)
	return nil, 200
}

func GetImageJSON(r *Request) (error, int) {
	jsonPath := r.storage.ImageJSONPath(r.imageId)
	jsonData, err := r.storage.GetContent(jsonPath)
	if err != nil {
		return errors.New("Image not found"), 404
	}
	size, _ := r.storage.GetSize(jsonPath)
	r.w.Header().Add("X-Docker-Size", fmt.Sprintf("%d", size))
	checksumPath := r.storage.ImageChecksumPath(r.imageId)
	if r.storage.Exists(checksumPath) {
		sumData, _ := r.storage.GetContent(checksumPath)
		r.w.Header().Add("X-Docker-Checksum", string(sumData))
	}
	r.w.Header().Add("Content-Type", "application/json")
	r.w.Write(jsonData)
	return nil, 200
}

func PutImageChecksum(r *Request) (error, int) {
	checksum := r.r.Header.Get("X-Docker-Checksum")
	if checksum == "" {
		return errors.New("Missing image's checksum"), 400
	}
	sessionChecksum := r.session.Values["checksum"]
	if sessionChecksum == "" {
		return errors.New("Missing Image's checksum"), 400
	}
	if !r.storage.Exists(r.storage.ImageJSONPath(r.imageId)) {
		return errors.New("Image not found"), 404
	}
	markPath := r.storage.ImageMarkPath(r.imageId)
	if !r.storage.Exists(markPath) {
		return errors.New("Cannot set this image checksum"), 409
	}
	err := storeChecksum(r)
	if err != nil {
		return err, 404
	}
	r.storage.Remove(markPath)
	return nil, 200
}

func storeChecksum(r *Request) error {
	checksum := r.r.Header.Get("X-Docker-Checksum")
	checksumPath := r.storage.ImageChecksumPath(r.imageId)
	if len(checksum) > 0 {
		checksumParts := strings.Split(checksum, ":")
		if len(checksumParts) != 2 {
			return errors.New("invalid checksum path")
		}
		r.storage.PutContent(checksumPath, []byte(checksum))
	} else {
		r.storage.Remove(checksumPath)
	}
	return nil
}

func generateAncestry(r *Request, parentID string) error {
	if parentID == "" {
		jsonData, _ := json.Marshal([]string{r.imageId})
		r.storage.PutContent(r.storage.ImageAncestryPath(r.imageId), jsonData)
		return nil
	}
	data, err := r.storage.GetContent(r.storage.ImageAncestryPath(parentID))
	if err != nil {
		return err
	}
	ancestry := []string{}
	err = json.Unmarshal(data, &ancestry)
	if err != nil {
		return err
	}
	ancestry = append([]string{r.imageId}, ancestry...)
	jsonData, _ := json.Marshal(ancestry)
	return r.storage.PutContent(r.storage.ImageAncestryPath(r.imageId), jsonData)
}

func getImage(r *Request, data []byte) (*models.ImageJSON, string, string, error) {
	jsonPath := r.storage.ImageJSONPath(r.imageId)
	markPath := r.storage.ImageMarkPath(r.imageId)
	image := &models.ImageJSON{}
	err := json.Unmarshal(data, image)
	if err != nil {
		return image, markPath, jsonPath, errors.New("invalid JSON")
	}
	if image.ID != r.imageId {
		return image, markPath, jsonPath, errors.New("JSON data contains an invalid id")
	}
	if image.Parent != "" && !r.storage.Exists(r.storage.ImageJSONPath(image.Parent)) {
		return image, markPath, jsonPath, errors.New("Image depends on a non existing parent")
	}

	if r.storage.Exists(jsonPath) && !r.storage.Exists(markPath) {
		return image, markPath, jsonPath, errors.New("Image already exists")
	}
	return image, markPath, jsonPath, nil
}

func storeImageJSON(r *Request) error {
	data, _ := r.readBody()
	image, markPath, jsonPath, err := getImage(r, data)
	if err != nil {
		return err
	}
	r.storage.PutContent(markPath, []byte("true"))
	r.storage.PutContent(jsonPath, data)
	generateAncestry(r, image.Parent)
	return nil
}

func in(word string, words []string) bool {
	for _, w := range words {
		if word == w {
			return true
		}
	}
	return false
}

// func StoreChecksum(imageId, checksum string) error {
// 	checksumParts := strings.Split(checksum, ":")
// 	if len(checksumParts) != 2 {
// 		return errors.New("Invalid checksum format")
// 	}
// 	checksumPath := r.storage.ImageChecksumPath(imageId)
// 	r.storage.PutContent(checksumPath, []byte(checksum))
// 	return nil
// }
