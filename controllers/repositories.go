package controllers

import (
	//"code.google.com/p/go.crypto/bcrypt"
	"fmt"
	//"io"
	"errors"
	"strings"
	//"net/http"
	//"bitbucket.org/cswank/docker-registry/storage"
	"bitbucket.org/cswank/docker-registry/models"
	"crypto/rand"
	"encoding/json"
	//"bitbucket.com/cswank/docker-registry/app/routes"
)

func putRepository(r *Request, images string) (error, int) {
	addTokenHeaders(r, images, "write")
	body, err := r.readBody()
	if err != nil {
		return errors.New("Invalid Data"), 400
	}
	data := []models.IndexImage{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return errors.New("Invalid Data"), 400
	}
	err = updateIndexImages(r, data)
	if err != nil {
		return errors.New("server error"), 500
	}

	return nil, 200
}

func PutRepository(r *Request) (error, int) {
	r.namespace = "library"
	return putRepository(r, "")
}

func PutRepositoryWithImages(r *Request) (error, int) {
	r.namespace = "library"
	return putRepository(r, "images")
}

func PutRepositoryWithNamespace(r *Request) (error, int) {
	return putRepository(r, "")
}

func PutRepositoryWithNamespaceAndImages(r *Request) (error, int) {
	return putRepository(r, "images")
}

func getRepository(r *Request) (error, int) {
	path := r.storage.IndexImagesPath(r.namespace, r.repository)
	data, err := r.storage.GetContent(path)
	if err != nil {
		return errors.New("Images not found"), 404
	}
	addTokenHeaders(r, "", "read")
	return r.writeJSON(data)
}

func GetRepository(r *Request) (error, int) {
	r.namespace = "library"
	return getRepository(r)
}

func GetRepositoryWithNamespace(r *Request) (error, int) {
	return getRepository(r)
}

func updateIndexImages(r *Request, indexImages []models.IndexImage) error {
	indexImagesPath := r.storage.IndexImagesPath(r.namespace, r.repository)
	existingIndexImages := []models.IndexImage{}
	indexImagesData, err := r.storage.GetContent(indexImagesPath)
	if err == nil {
		err = json.Unmarshal(indexImagesData, &existingIndexImages)
		if err != nil {
			return errors.New(fmt.Sprintf("error unmarshaling existing indexImages: %s", string(indexImagesData)))
		}
	}
	return saveIndexImages(r, indexImages, existingIndexImages, indexImagesPath)
}

func saveIndexImages(r *Request, images, existingImages []models.IndexImage, indexImagesPath string) error {
	values := filterIndexImages(images, existingImages)
	b, err := json.Marshal(values)
	if err == nil {
		err = r.storage.PutContent(indexImagesPath, b)
	}
	return err
}

func filterIndexImages(indexImages, existingIndexImages []models.IndexImage) []models.IndexImage {
	outputImages := map[string]models.IndexImage{}
	indexImages = append(indexImages, existingIndexImages...)
	for _, image := range indexImages {
		i, ok := outputImages[image.ID]
		if ok && i.Checksum != "" {
			continue
		}
		outputImages[image.ID] = image
	}
	return valuesFromMap(outputImages)
}

func valuesFromMap(m map[string]models.IndexImage) []models.IndexImage {
	o := make([]models.IndexImage, len(m))
	i := 0
	for _, value := range m {
		o[i] = value
		i++
	}
	return o
}

func randomString(n int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	var bytes = make([]byte, n)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func addTokenHeaders(r *Request, images, access string) {
	token := fmt.Sprintf(
		`Token signature=%s,repository="%s/%s",access=%s`,
		randomString(16),
		r.namespace,
		r.repository,
		access,
	)
	r.w.Header().Set("X-Docker-Endpoints", r.r.Host)
	r.w.Header().Set("X-Docker-Token", token)
	r.w.Header().Set("WWW-Authenticate", token)
	r.w.Header().Set("Cache-Control", "no-cache")
	if len(images) > 0 {
		r.w.WriteHeader(204)
	} else {
		r.w.WriteHeader(200)
	}
}

func parseRepository(repository string) (namespace, repo, images string, err error) {
	parts := strings.Split(repository, "/")
	if len(parts) == 1 {
		namespace = "library"
		repo = repository
	} else if len(parts) == 2 {
		namespace = parts[0]
		repo = parts[1]
	} else if len(parts) == 3 && parts[2] == "images" {
		namespace = parts[0]
		repo = parts[1]
		images = parts[2]
	} else {
		err = errors.New("could not parse the repository")
	}
	return namespace, repo, images, err
}
