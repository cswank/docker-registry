package controllers

import (
	"bitbucket.org/cswank/docker-registry/models"
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	//"bitbucket.org/cswank/docker-registry/checksums"
)

var (
	testRepoData      = []byte(`[{"id": "27cf784147099545"}, {"Tag": "latest", "id": "b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc"}]`)
	testImageJSONData = []byte(`{"comment": "Imported from http://get.docker.io/images/base", "container_config": {"Tty": false, "Cmd": null, "Env": null, "Image": "", "Hostname": "", "User": "", "MemorySwap": 0, "Memory": 0, "Detach": false, "Ports": null, "OpenStdin": false}, "id": "27cf784147099545", "created": "2013-03-23T12:53:11.10432-07:00"}`)
	testImageJSON     *models.ImageJSON
	testRepo          []models.IndexImage
)

func init() {
	json.Unmarshal(testImageJSONData, &testImageJSON)
	json.Unmarshal(testRepoData, &testRepo)
}

func getTestWriter() *httptest.ResponseRecorder {
	if cfg["storage_path"] == "" {
		tmp, _ := ioutil.TempDir("", "")
		cfg = map[string]string{
			"storage_path": tmp,
		}
	}
	return httptest.NewRecorder()
}

func getTestRequest(method, url string, body io.Reader) (*Request, *httptest.ResponseRecorder) {
	w := getTestWriter()
	r, _ := http.NewRequest(method, url, body)
	vars := map[string]string{
		"imageId":    "27cf784147099545",
		"namespace":  "library",
		"repository": "base",
	}
	return NewRequest(w, r, vars), w
}

func cleanupTest() {
	if cfg["storage_path"] != "" {
		os.RemoveAll(cfg["storage_path"])
	}
	cfg["storage_path"] = ""
}

func TestPutImageJSON(t *testing.T) {
	cleanupTest()
	req, _ := getTestRequest("PUT", "http://example.com/v1/images/27cf784147099545/json", bytes.NewBuffer(testImageJSONData))
	err, status := PutImageJSON(req)
	if err != nil {
		t.Error(err)
	}
	if status != 200 {
		t.Error(status)
	}
	cleanupTest()
}

func TestGetImageJSON(t *testing.T) {
	cleanupTest()
	req, _ := getTestRequest("PUT", "http://example.com/v1/images/27cf784147099545/json", bytes.NewBuffer(testImageJSONData))
	PutImageJSON(req)
	req, _ = getTestRequest("GET", "http://example.com/v1/images/27cf784147099545/json", nil)
	GetImageJSON(req)
	cleanupTest()
}

func TestPutImage(t *testing.T) {
	cleanupTest()
	req, _ := getTestRequest("PUT", "http://example.com/v1/images/27cf784147099545/json", bytes.NewBuffer(testImageJSONData))
	PutImageJSON(req)
	f, _ := os.Open("test_data/layer.tar.gz")
	req, _ = getTestRequest("PUT", "http://example.com/v1/images/27cf784147099545/layer", f)
	err, status := PutImageLayer(req)
	if err != nil {
		t.Error(err)
	}
	if status != 200 {
		t.Error(status)
	}
	//cleanupTest()
}

// func TestPutImageLayer(t *testing.T) {
// 	req, _ := getTestRequest("PUT", "http://example.com/v1/images/xyz/layer", bytes.NewBufferString("howdy folks"))
// 	err, status := PutImageLayer(req)
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	if status != 200 {
// 		t.Error(status)
// 	}
// }

// func TestBenchmarkRender(t *testing.T) {
// 	startFakeBookingApp()
// 	resp := httptest.NewRecorder()
// 	c := NewController(NewRequest(showRequest), NewResponse(resp))
// 	c.SetAction("Hotels", "Show")
// 	result := Hotels{c}.Show(3)
// 	result.Apply(c.Request, c.Response)
// 	if !strings.Contains(resp.Body.String(), "300 Main St.") {
// 		t.Errorf("Failed to find hotel address in action response:\n%s", resp.Body)
// 	}
// }

// func getTestImages(requestPath string) (*Images, *httptest.ResponseRecorder, string) {

// 	req, _ := http.NewRequest("GET", requestPath, nil)
// 	c := revel.NewController(revel.NewRequest(req), revel.NewResponse(resp))
// 	a := Application{c}
// 	s := StorageApplication{
// 		Application: a,
// 		Storage: l,
// 	}
// 	return &Images{StorageApplication: s}, resp, tmp
// }

/*
here are the requests that are made when an image is pushed to the registry

% gunicorn --access-logfile - --debug -k gevent -b 0.0.0.0:5000 -w 1 wsgi:application                                                                                                    13-08-27 - 16:45:47
2013-08-27 16:46:18 [17339] [INFO] Starting gunicorn 0.17.4
2013-08-27 16:46:18 [17339] [INFO] Listening at: http://0.0.0.0:5000 (17339)
2013-08-27 16:46:18 [17339] [INFO] Using worker: gevent
2013-08-27 16:46:18 [17344] [INFO] Booting worker with pid: 17344

ping
"127.0.0.1 - - [30/Aug/2013:13:53:27] "GET /v1/_ping HTTP/1.1" 200 4 "-" "Go 1.1 package http"
2013-08-30 13:53:27,765 INFO: "127.0.0.1 - - [30/Aug/2013:13:53:27] "GET /v1/_ping HTTP/1.1" 200 4 "-" "Go 1.1 package http"
2013-08-30 13:53:27,767 DEBUG: check_session: Session is empty
put repository
"127.0.0.1 - - [30/Aug/2013:13:53:27] "PUT /v1/repositories/base/ HTTP/1.1" 200 2 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:27,769 INFO: "127.0.0.1 - - [30/Aug/2013:13:53:27] "PUT /v1/repositories/base/ HTTP/1.1" 200 2 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:27,770 DEBUG: check_session: Session is empty
get image json
2013-08-30 13:53:27,771 DEBUG: api_error: Image not found
"127.0.0.1 - - [30/Aug/2013:13:53:27] "GET /v1/images/27cf784147099545/json HTTP/1.1" 404 34 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:27,771 INFO: "127.0.0.1 - - [30/Aug/2013:13:53:27] "GET /v1/images/27cf784147099545/json HTTP/1.1" 404 34 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:27,773 DEBUG: check_session: Session is empty
put image json
"127.0.0.1 - - [30/Aug/2013:13:53:27] "PUT /v1/images/27cf784147099545/json HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:27,775 INFO: "127.0.0.1 - - [30/Aug/2013:13:53:27] "PUT /v1/images/27cf784147099545/json HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:53:28,681 DEBUG: check_session: Session is empty
put image layer
2013-08-30 13:54:06,064 DEBUG: checksums.compute_tarsum: return tarsum+sha256:ef285927a38d1bbdcc2522c501b30edd0bf9135eeae2ee0cd8e38ba17fa49d36
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/27cf784147099545/layer HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,080 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/27cf784147099545/layer HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
put image checksum
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/27cf784147099545/checksum HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,360 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/27cf784147099545/checksum HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,364 DEBUG: [put_tag] namespace=library; repository=base; tag=latest
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/tags/latest HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,692 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/tags/latest HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
get image json
2013-08-30 13:54:06,696 DEBUG: api_error: Image not found
"127.0.0.1 - - [30/Aug/2013:13:54:06] "GET /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/json HTTP/1.1" 404 34 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,698 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "GET /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/json HTTP/1.1" 404 34 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
put image json
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/json HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,703 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/json HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
put image layer
2013-08-30 13:54:06,721 DEBUG: checksums.compute_tarsum: return tarsum+sha256:4be36c041020d529a6300a4eb9e75d7c02d54aa7192f2e7e798f5b256decfc1a
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/layer HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,723 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/layer HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
put image checksum
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/checksum HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,726 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/images/b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc/checksum HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,728 DEBUG: [put_tag] namespace=library; repository=base; tag=latest
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/tags/latest HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,730 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/tags/latest HTTP/1.1" 200 4 "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
put repository
"127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/images HTTP/1.1" 204 - "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"
2013-08-30 13:54:06,733 INFO: "127.0.0.1 - - [30/Aug/2013:13:54:06] "PUT /v1/repositories/base/images HTTP/1.1" 204 - "-" "docker/0.6.1 go/go1.1.2 git-commit/5105263 kernel/3.8.0-29-generic"

results in:

├── images
│   ├── 27cf784147099545
│   │   ├── ancestry
│   │   ├── _checksum
│   │   ├── json
│   │   └── layer
│   └── b750fe79269d2ec9a3c593ef05b4332b1d1a02a62b4accb2c21d589ff2f5f2dc
│       ├── ancestry
│       ├── _checksum
│       ├── json
│       └── layer
└── repositories
    └── library
        └── base
            ├── _index_images
            └── tag_latest
*/
