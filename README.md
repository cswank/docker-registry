Docker Registry
=============
Running docker-registry:

    $ cd docker-registry
    $ go get
    $ go install
    $ cd $GOPATH/bin
    $ ./docker-registry


The default storage is local storage (on the filesystem).  The default
location for the local storage is /tmp/docker-registry.  To change
the location to something else set REGISTRY_STORAGE_PATH in your environment:
To use local storage set 

    $ export REGISTRY_STORAGE_PATH=/opt/docker-registry

To use mongodb storage instead set REGISTRY_STORAGE to mongo:

    $ export REGISTRY_STORAGE=mongo

The default db is 'registry' and the default host is 'localhost'.  To change either of those
set the following:

    $ export REGISTRY_STORAGE_DB=mydb
    $ export REGISTRY_STORAGE_HOST=somemongohost

Once your env is set up then run the server:

    $ $GOPATH/bin/docker-registry

