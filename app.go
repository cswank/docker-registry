package main

import (
	"bitbucket.org/cswank/docker-registry/controllers"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type controller func(*controllers.Request) (error, int)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/_ping", Ping).Methods("GET")
	r.HandleFunc("/v1/_ping", Ping).Methods("GET")

	r.HandleFunc("/v1/repositories/{repository}/", PutRepository).Methods("PUT")
	r.HandleFunc("/v1/repositories/{repository}/images", PutRepositoryWithImages).Methods("PUT")
	r.HandleFunc("/v1/repositories/{repository}/images", GetRepository).Methods("GET")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}", PutRepositoryWithNamespace).Methods("PUT")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}/images", PutRepositoryWithNamespaceAndImages).Methods("PUT")

	r.HandleFunc("/v1/images/{imageId}/checksum", PutImageChecksum).Methods("PUT")
	r.HandleFunc("/v1/images/{imageId}/layer", PutImageLayer).Methods("PUT")
	r.HandleFunc("/v1/images/{imageId}/json", PutImageJSON).Methods("PUT")

	r.HandleFunc("/v1/images/{imageId}/json", GetImageJSON).Methods("GET")
	r.HandleFunc("/v1/images/{imageId}/layer", GetImageLayer).Methods("GET")
	r.HandleFunc("/v1/images/{imageId}/ancestry", GetImageAncestry).Methods("GET")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}/images", GetRepositoryWithNamespace).Methods("GET")

	r.HandleFunc("/v1/repositories/{repository}/tags/{tag}", GetTag).Methods("GET")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}/tags", GetTagsWithNamespace).Methods("GET")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}/tags/{tag}", GetTagWithNamespace).Methods("GET")

	r.HandleFunc("/v1/repositories/{repository}/tags/{tag}", PutTag).Methods("PUT")
	r.HandleFunc("/v1/repositories/{namespace}/{repository}/tags/{tag}", PutTagWithNamespace).Methods("PUT")

	http.Handle("/", r)
	fmt.Println("listening on 0.0.0.0:5000")
	http.ListenAndServe(":5000", nil)
}

func Ping(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.Ping)
}

func PutImageLayer(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutImageLayer)
}

func PutImageChecksum(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutImageChecksum)
}

func PutImageJSON(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutImageJSON)
}

func GetImageJSON(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetImageJSON)
}

func GetImageLayer(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetImageLayer)
}

func GetImageAncestry(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetImageAncestry)
}

func PutRepository(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutRepository)
}

func PutRepositoryWithImages(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutRepositoryWithImages)
}

func PutRepositoryWithNamespace(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutRepositoryWithNamespace)
}

func PutRepositoryWithNamespaceAndImages(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutRepositoryWithNamespaceAndImages)
}

func GetRepository(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetRepository)
}

func GetRepositoryWithNamespace(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetRepositoryWithNamespace)
}

func GetTag(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetTag)
}

func GetTagsWithNamespace(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetTagsWithNamespace)
}

func GetTagWithNamespace(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.GetTagWithNamespace)
}

func PutTagWithNamespace(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutTagWithNamespace)
}

func PutTag(w http.ResponseWriter, r *http.Request) {
	handle(w, r, controllers.PutTag)
}

func handle(w http.ResponseWriter, r *http.Request, ctrl controller) {
	w.Header().Add("X-Frame-Options", "SAMEORIGIN")
	w.Header().Add("X-XSS-Protection", "1; mode=block")
	w.Header().Add("X-Content-Type-Options", "nosniff")
	w.Header().Add("X-Docker-Registry-Version", "0.1")
	w.Header().Add("X-Docker-Registry-Config", "dev")
	w.Header().Add("Cache-Control", "no-cache")
	w.Header().Add("Pragma", "no-cache")
	w.Header().Add("Expires", "-1")
	vars := mux.Vars(r)
	req, e := controllers.NewRequest(w, r, vars)
	if e != nil {
		log.Println(e)
		w.WriteHeader(500)
		return
	}
	err, status := ctrl(req)
	if err != nil {
		log.Println(err, status)
		w.WriteHeader(status)
	}
}
