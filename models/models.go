package models

type ReadCloseSeeker interface {
	Read([]byte) (int, error)
	Close() error
	Seek(int64, int) (int64, error)
}

/*
{
  'comment': 'Imported from http://get.docker.io/images/base',
  'container_config': {'Tty': False, 'Cmd': None, 'Env': None, 'Image': u'', 'Hostname': u'', 'User': u'', 'MemorySwap': 0, 'Memory': 0, 'Detach': False, 'Ports': None, 'OpenStdin': False},
  'id': '27cf784147099545',
  'created': '2013-03-23T12:53:11.10432-07:00',
}
*/

type ImageJSON struct {
	ID              string                 `json:"id"`
	Container       string                 `json:"container"`
	Created         string                 `json:"created"`
	Comment         string                 `json:"comment"`
	Parent          string                 `json:"parent"`
	ContainerConfig map[string]interface{} `json:"container_config"`
}

type IndexImage struct {
	ID       string `json:"id"`
	Tag      string `json:"Tag"`
	Checksum string `json:"checksum"`
}
